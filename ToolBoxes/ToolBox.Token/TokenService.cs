﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Reflection;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Web;

namespace ToolBox.Token
{
    public class TokenService : ITokenService
    {
        private readonly SymmetricSecurityKey _SecurityKey;
        private readonly JwtSecurityTokenHandler _Handler;
        private readonly string _Issuer;
        private readonly string _Audience;

        public TokenService()
        {
            _Handler = new JwtSecurityTokenHandler();
            _SecurityKey 
                = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(ConfigurationManager.AppSettings.Get("Signature")));
            _Issuer = ConfigurationManager.AppSettings.Get("Issuer");
            _Audience = ConfigurationManager.AppSettings.Get("Audience");
        }

        public string GenerateToken<T>(T payload)
        {
            SigningCredentials credentials = new SigningCredentials(_SecurityKey, SecurityAlgorithms.HmacSha256);

            JwtSecurityToken secToken = new JwtSecurityToken(
                _Issuer,
                _Audience,
                ToClaims(payload),
                DateTime.Now,
                DateTime.Now.AddDays(1),
                credentials                
            );
            return _Handler.WriteToken(secToken);
        }

        public ClaimsPrincipal ValidateToken(string authToken)
        {
            TokenValidationParameters validationParameters = GetValidationParameters();
            return _Handler.ValidateToken(authToken, validationParameters, out SecurityToken validatedToken);
        }

        private TokenValidationParameters GetValidationParameters()
        {
            return new TokenValidationParameters()
            {
                ValidateLifetime = true,
                ValidateIssuer = true,
                ValidateAudience = false,
                ValidIssuer = _Issuer,
                RequireSignedTokens = true,
                IssuerSigningKey = _SecurityKey
            };
        }

        private IEnumerable<Claim> ToClaims<T>(T payload)
        {
            foreach (PropertyInfo prop in typeof(T).GetProperties().Where(p => p.GetValue(payload) != null))
            {
                yield return new Claim(prop.Name, prop.GetValue(payload).ToString());
            }
        }
    }
}