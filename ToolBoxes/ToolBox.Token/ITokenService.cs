﻿using System.Security.Claims;

namespace ToolBox.Token
{
    public interface ITokenService
    {
        string GenerateToken<T>(T payload);
        ClaimsPrincipal ValidateToken(string authToken);
    }
}