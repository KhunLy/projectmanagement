﻿using System;
using System.Linq;
using System.Reflection;

namespace ToolBox.Mapper
{
    public static class MapperExtension
    {
        public static T MapTo<T>(this object from, params object[] ctorParameters)
            where T : class
        {
            if (from == null)
                return null;
            ConstructorInfo ctor = typeof(T).GetConstructor(ctorParameters.Select(p => p.GetType()).ToArray());
            if (ctor == null)
                throw new ArgumentException();
            T result = (T)ctor.Invoke(ctorParameters);
            foreach (PropertyInfo toProp in typeof(T).GetProperties())
            {
                PropertyInfo fromProp = from.GetType().GetProperty(toProp.Name);
                if (toProp.PropertyType.IsAssignableFrom(fromProp.PropertyType))
                    toProp.SetValue(result, fromProp.GetValue(from));
            }
            return result;
        }
    }
}
