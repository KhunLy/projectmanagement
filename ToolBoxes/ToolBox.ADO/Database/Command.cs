﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.ADO.Database
{
    public class Command
    {
        internal string Query { get; }
        internal Dictionary<string, object> Parameters { get; }
        internal bool IsStoredProcedure { get; }

        public Command(string query, bool isStoredProcedure = false)
        {
            Query = query;
            Parameters = new Dictionary<string, object>();
            IsStoredProcedure = isStoredProcedure;
        }
        public void AddParameter(string paramName, object value)
        {
            Parameters.Add(paramName, value);
        }
    }
}
