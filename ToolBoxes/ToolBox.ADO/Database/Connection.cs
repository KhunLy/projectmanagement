﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolBox.ADO.Database
{
    public class Connection
    {
        private readonly string _ConnectionString;

        private readonly DbProviderFactory _Factory;

        public Connection(string connectionString, string providerInvariantName)
        {
            _Factory = DbProviderFactories.GetFactory(providerInvariantName);
            _ConnectionString = connectionString;
        }

        public IDbConnection CreateConnection()
        {
            IDbConnection connection = _Factory.CreateConnection();
            connection.ConnectionString = _ConnectionString;
            return connection;
        }


        public IDbCommand CreateCommand(Command cmd, IDbConnection conn)
        {
            IDbCommand command = conn.CreateCommand();
            command.CommandText = cmd.Query;
            command.CommandType = cmd.IsStoredProcedure 
                ? CommandType.StoredProcedure : CommandType.Text;
            foreach (KeyValuePair<string,object> kvp in cmd.Parameters)
            {
                IDbDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = kvp.Key;
                parameter.Value = kvp.Value ?? DBNull.Value;
                command.Parameters.Add(parameter);
            }
            return command;
        }

        public IEnumerable<T> ExecuteReader<T>(Command command, Func<IDataRecord, T> selector)
            where T: class
        {
            using (IDbConnection conn = CreateConnection())
            {
                conn.Open();
                using (IDbCommand cmd = CreateCommand(command, conn))
                {
                    using (IDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            yield return selector(reader);
                        }
                    }
                }
            }
        }

        public object ExecuteScalar(Command command)
        {
            using (IDbConnection conn = CreateConnection())
            {
                conn.Open();
                using (IDbCommand cmd = CreateCommand(command, conn))
                { 
                    return cmd.ExecuteScalar(); 
                }
            }
        }

        public int ExecuteNonQuery(Command command)
        {
            using (IDbConnection conn = CreateConnection())
            {
                conn.Open();
                using (IDbCommand cmd = CreateCommand(command, conn))
                {
                    return cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
