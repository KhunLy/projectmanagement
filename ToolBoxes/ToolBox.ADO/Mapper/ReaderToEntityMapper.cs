﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using ToolBox.ADO.CustomAttributes;

namespace ToolBox.ADO.Mapper
{
    public static class ReaderToEntityMapper
    {
        public static T MapTo<T>(IDataRecord record)
            where T: class, new()
        {
            T result = Activator.CreateInstance<T>();
            foreach (PropertyInfo prop in typeof(T).GetProperties())
            {
                object value = record[prop.GetCustomAttribute<ColumnAttribute>()?.ColumnName ?? prop.Name];
                if (prop.PropertyType.IsAssignableFrom(value.GetType()))
                    prop.SetValue(result, value);
            }
            return result;
        }
    }
}
