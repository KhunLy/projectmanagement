﻿using System.Threading.Tasks;

namespace ProjectManagement.BLL.Google
{
    public interface IGoogleValidator
    {
        Task<GooglePayload> ValidateToken(string token);
    }
}