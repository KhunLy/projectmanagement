﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Apis.Auth;
using Google.Apis.Auth.OAuth2;
using ToolBox.Mapper;

namespace ProjectManagement.BLL.Google
{
    public class GoogleValidator : IGoogleValidator
    {
        public async Task<GooglePayload> ValidateToken(string token)
        {
            GoogleJsonWebSignature.Payload payload = await GoogleJsonWebSignature.ValidateAsync(token);
            return payload.MapTo<GooglePayload>();
        }
    }
}
