﻿using ProjectManagement.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.BLL.Models
{
    public class Task: ITask
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Weight { get; set; }
        public string Description { get; set; }
        public int Tag { get; set; }
        public int StatusID { get; set; }
        public bool Deleted { get; set; }
    }
}
