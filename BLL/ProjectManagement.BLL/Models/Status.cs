﻿using ProjectManagement.BLL.Repositories;
using ProjectManagement.BLL.UOW;
using ProjectManagement.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectManagement.BLL.Models
{
    public class Status : IStatus
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Weight { get; set; }
        public string Color { get; set; }
        public int ProjectID { get; set; }
        public bool Deleted { get; set; }

        private IEnumerable<Task> _Tasks;
        public IEnumerable<Task> Tasks
        {
            get
            {
                if (_Tasks != null)
                    return _Tasks;
                using (UnitOfWork uow = new UnitOfWork())
                {
                    return _Tasks = uow.GetRepository<ITaskRepository>().GetAllByStatusId(ID);
                }
            }
        }
    }
}
