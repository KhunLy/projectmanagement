﻿using Autofac;
using ProjectManagement.ADO.UOW;
using ProjectManagement.BLL.Repositories;
using ProjectManagement.DAL.UOW;
using System;

namespace ProjectManagement.BLL.UOW
{
    public class UnitOfWork : IDisposable
    {
        private readonly IContainer _Container;

        public UnitOfWork()
        {
            _Container = CreateContainer();
        }

        private IContainer CreateContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterInstance(new ADOUnitOfWork()).As<IUnitOfWork>();
            //builder.RegisterInstance(new EFUnitOfWork()).As<IUnitOfWork>();
            builder.RegisterType<TaskRepository>().As<ITaskRepository>().SingleInstance();
            return builder.Build();
        }

        public T GetRepository<T>()
        {
            return _Container.Resolve<T>();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _Container.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~UnitOfWork()
        {
            Dispose(false);
        }
    }
}
