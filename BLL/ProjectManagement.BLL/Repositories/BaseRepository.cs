﻿using ProjectManagement.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.BLL.Repositories
{
    class BaseRepository
    {
        protected readonly IUnitOfWork _UOW;
        public BaseRepository(IUnitOfWork uOW)
        {
            _UOW = uOW;
        }
    }
}
