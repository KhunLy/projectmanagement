﻿using ProjectManagement.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectManagement.BLL.Repositories
{
    public interface ITaskRepository
    {
        Task Get(int id);
        IEnumerable<Task> GetAll();
        IEnumerable<Task> GetAllByStatusId(int statusId);
        void Insert(Task t);
        void Update(Task t);
        void Delete(int id);
    }
}
