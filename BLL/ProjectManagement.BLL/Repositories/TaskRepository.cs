﻿using ProjectManagement.BLL.Models;
using ProjectManagement.DAL.UOW;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ToolBox.Mapper;

namespace ProjectManagement.BLL.Repositories
{
    class TaskRepository: BaseRepository, ITaskRepository
    {
        public TaskRepository(IUnitOfWork uOW)
            : base(uOW) { }

        public void Delete(int id)
        {
            _UOW.GetRepository<DAL.Repositories.ITaskRepository>()
                .Delete(id);
        }

        public Task Get(int id)
        {
            return _UOW.GetRepository<DAL.Repositories.ITaskRepository>()
                .Get(id).MapTo<Task>();
        }

        public IEnumerable<Task> GetAll()
        {
            return _UOW.GetRepository<DAL.Repositories.ITaskRepository>()
                .GetAll().Select(t => t.MapTo<Task>());
        }

        public IEnumerable<Task> GetAllByStatusId(int statusId)
        {
            return _UOW.GetRepository<DAL.Repositories.ITaskRepository>()
                .GetAllByStatusId(statusId).Select(t => t.MapTo<Task>());
        }

        public void Insert(Task t)
        {
            _UOW.GetRepository<DAL.Repositories.ITaskRepository>()
                .Insert(t);
        }

        public void Update(Task t)
        {
            _UOW.GetRepository<DAL.Repositories.ITaskRepository>()
                .Update(t);
        }
    }
}
