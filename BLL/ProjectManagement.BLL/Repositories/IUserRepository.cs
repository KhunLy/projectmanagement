﻿using ProjectManagement.BLL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectManagement.BLL.Repositories
{
    public interface IUserRepository
    {
        User Get(int id);
        IEnumerable<User> GetAll();
        User GetByEmailAndPassword(string email, string password);
        User GetByEmail(string email);
        void Insert(User t);
        void Update(User t);
        void Delete(int id);
    }
}
