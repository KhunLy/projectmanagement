﻿using ProjectManagement.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.ADO.Models
{
    class Status: IStatus
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Weight { get; set; }
        public string Color { get; set; }
        public int ProjectID { get; set; }
        public bool Deleted { get; set; }
    }
}
