﻿using ProjectManagement.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.ADO.Models
{
    class Project: IProject
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public bool Deleted { get; set; }
    }
}
