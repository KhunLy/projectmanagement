﻿using ProjectManagement.ADO.Models;
using ProjectManagement.DAL.Models;
using ProjectManagement.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ToolBox.ADO.Database;
using ToolBox.ADO.Mapper;

namespace ProjectManagement.ADO.Repositories
{
    class TaskRepository : ITaskRepository
    {
        private readonly Connection _Connection;

        public TaskRepository(Connection connection)
        {
            _Connection = connection;
        }

        public void Delete(int key)
        {
            Command cmd = new Command("DELETE FROM Task WHERE ID = @id");
            cmd.AddParameter("@id", key);
            _Connection.ExecuteNonQuery(cmd);
        }

        public ITask Get(int key)
        {
            Command cmd = new Command("SELECT * FROM Task WHERE ID = @id");
            cmd.AddParameter("@id", key);
            return _Connection.ExecuteReader(cmd, ReaderToEntityMapper.MapTo<Task>).FirstOrDefault();
        }

        public IEnumerable<ITask> GetAll()
        {
            Command cmd = new Command("SELECT * FROM Task");
            return _Connection.ExecuteReader(cmd, ReaderToEntityMapper.MapTo<Task>);
        }

        public IEnumerable<ITask> GetAllByStatusId(int statusId)
        {
            Command cmd = new Command("sp_get_tasks_by_status", true);
            cmd.AddParameter("@projectID", statusId);
            return _Connection.ExecuteReader<Task>(cmd, ReaderToEntityMapper.MapTo<Task>);
        }

        public void Insert(ITask entity)
        {
            //Command cmd = new Command("INSERT INTO Task() VALUES ")
        }

        public void Update(ITask entity)
        {
            throw new NotImplementedException();
        }
    }
}
