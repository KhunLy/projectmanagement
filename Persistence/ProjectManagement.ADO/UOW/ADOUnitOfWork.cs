﻿using Autofac;
using ProjectManagement.ADO.Repositories;
using ProjectManagement.DAL.Repositories;
using ProjectManagement.DAL.UOW;
using System;
using System.Configuration;
using System.Reflection;
using System.Transactions;
using ToolBox.ADO.Database;

namespace ProjectManagement.ADO.UOW
{
    public class ADOUnitOfWork : IUnitOfWork
    {
        private TransactionScope _Transaction;

        private readonly IContainer _Container;

        public ADOUnitOfWork()
        {
            _Transaction = CreateTransaction();
            _Container = CreateContainer();
        }

        private TransactionScope CreateTransaction()
        {
            return new TransactionScope(
                TransactionScopeOption.Required,
                new TransactionOptions {
                    IsolationLevel = IsolationLevel.Serializable
                }
            );
        }

        private IContainer CreateContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterInstance(new Connection(
                ConfigurationManager.ConnectionStrings["ADO_default"].ConnectionString,
                ConfigurationManager.ConnectionStrings["ADO_default"].ProviderName
            ));
            builder.RegisterType<TaskRepository>().As<ITaskRepository>().SingleInstance();
            return builder.Build();
        }

        public TRepository GetRepository<TRepository>()
        {
            return _Container.Resolve<TRepository>();
        }

        public void SaveChanges()
        {
            try
            {
                _Transaction.Complete();
            }
            catch (TransactionAbortedException)
            {
                throw;
            }
            finally
            {
                _Transaction.Dispose();
                _Transaction = CreateTransaction();
            }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _Transaction.Dispose();
                    _Container.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~ADOUnitOfWork()
        {
            Dispose(false);
        }
    }
}
