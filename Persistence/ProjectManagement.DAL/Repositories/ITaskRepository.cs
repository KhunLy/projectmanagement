﻿using ProjectManagement.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.DAL.Repositories
{
    public interface ITaskRepository: IRepository<ITask, int>
    {
        IEnumerable<ITask> GetAllByStatusId(int projectId);
    }
}
