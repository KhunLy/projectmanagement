﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.DAL.Models
{
    public interface IStatus
    {
        int ID { get; set; }
        string Title { get; set; }
        int Weight { get; set; }
        string Color { get; set; }
        int ProjectID { get; set; }
        bool Deleted { get; set; }
    }
}
