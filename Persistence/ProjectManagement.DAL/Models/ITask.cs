﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.DAL.Models
{
    public interface ITask
    {
        int ID { get; set; }
        string Title { get; set; }
        int Weight { get; set; }
        string Description { get; set; }
        int Tag { get; set; }
        int StatusID { get; set; }
        bool Deleted { get; set; }
    }
}
