﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.DAL.Models
{
    public interface IUser
    {
        int ID { get; set; }
        string Lastname { get; set; }
        string Firstname { get; set; }
        string Username { get; set; }
        string Email { get; set; }
        string Password { get; set; }
        byte[] Picture { get; set; }
        DateTime InscriptionDate { get; set; }
        DateTime? LastConnectionDate { get; set; }
    }
}
