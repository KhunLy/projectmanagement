﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.DAL.Models
{
    public interface IProject
    {
        int ID { get; set; }
        string Title { get; set; }
        bool Deleted { get; set; }
    }
}
