﻿using ProjectManagement.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.DAL.UOW
{
    public interface IUnitOfWork: IDisposable
    {
        TRepository GetRepository<TRepository>();
        void SaveChanges();
    }
}
