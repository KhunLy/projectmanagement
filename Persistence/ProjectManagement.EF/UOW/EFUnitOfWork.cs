﻿using Autofac;
using ProjectManagement.DAL.Repositories;
using ProjectManagement.DAL.UOW;
using ProjectManagement.EF.EDMX;
using ProjectManagement.EF.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.EF.UOW
{
    public class EFUnitOfWork : IUnitOfWork
    {
        private readonly ProjectManagementEntities _Context;

        private readonly IContainer _Container;

        public EFUnitOfWork()
        {
            _Context = new ProjectManagementEntities();
            _Container = CreateContainer();
        }

        private IContainer CreateContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();
            builder.RegisterInstance(_Context);
            builder.RegisterType<TaskRepository>().As<ITaskRepository>().SingleInstance();
            //builder.RegisterType<ProjectRepository>().As<IProjectRepository>().SingleInstance();
            //builder.RegisterType<StatusRepository>().As<IStatusRepository>().SingleInstance();
            return builder.Build();
        }

        public TRepository GetRepository<TRepository>()
        {
            return _Container.Resolve<TRepository>();
        }

        public void SaveChanges()
        {
            _Context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _Context.Dispose();
                    _Container.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~EFUnitOfWork()
        {
            Dispose(false);
        }
    }
}
