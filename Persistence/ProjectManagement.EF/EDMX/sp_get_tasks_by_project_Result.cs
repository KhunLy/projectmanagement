//------------------------------------------------------------------------------
// <auto-generated>
//     Ce code a été généré à partir d'un modèle.
//
//     Des modifications manuelles apportées à ce fichier peuvent conduire à un comportement inattendu de votre application.
//     Les modifications manuelles apportées à ce fichier sont remplacées si le code est régénéré.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ProjectManagement.EF.EDMX
{
    using System;
    
    public partial class sp_get_tasks_by_project_Result
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Weight { get; set; }
        public string Description { get; set; }
        public int Tag { get; set; }
        public System.DateTime CreationDate { get; set; }
        public bool Deleted { get; set; }
        public Nullable<int> AssignedToID { get; set; }
        public Nullable<int> CreatorID { get; set; }
        public int StatusID { get; set; }
    }
}
