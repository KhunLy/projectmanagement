﻿using ProjectManagement.DAL.Models;
using ProjectManagement.DAL.Repositories;
using ProjectManagement.EF.EDMX;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectManagement.EF.Repositories
{
    class TaskRepository : GenericRepository<ITask,Task,int>, ITaskRepository
    {
        public TaskRepository(ProjectManagementEntities context) : base(context)
        {
        }

        public IEnumerable<ITask> GetAllByStatusId(int projectId)
        {
            throw new NotImplementedException();
        }
    }
}
