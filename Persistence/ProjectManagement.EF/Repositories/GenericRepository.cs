﻿using ProjectManagement.EF.EDMX;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace ProjectManagement.EF.Repositories
{
    class GenericRepository<TInterface,TEntity,TKey>
        where TEntity : class, TInterface
        where TKey: struct
    {
        protected readonly ProjectManagementEntities _Context;
        protected readonly DbSet<TEntity> dbSet;

        public GenericRepository(ProjectManagementEntities context)
        {
            _Context = context;
            this.dbSet = context.Set<TEntity>();
        }

        public virtual IEnumerable<TInterface> GetAll()
        {
            return dbSet;
        }

        public virtual TInterface Get(TKey id)
        {
            return dbSet.Find(id);
        }

        public virtual void Insert(TInterface entity)
        {
            dbSet.Add((TEntity)entity);
        }

        public virtual void Delete(TKey id)
        {
            TEntity entityToDelete = dbSet.Find(id);
            Delete(entityToDelete);
        }

        public virtual void Delete(TInterface entityToDelete)
        {
            if (_Context.Entry((TEntity)entityToDelete).State == EntityState.Detached)
            {
                dbSet.Attach((TEntity)entityToDelete);
            }
            dbSet.Remove((TEntity)entityToDelete);
        }

        public virtual void Update(TInterface entityToUpdate)
        {
            dbSet.Attach((TEntity)entityToUpdate);
            _Context.Entry((TEntity)entityToUpdate).State = EntityState.Modified;
        }
    }
}
