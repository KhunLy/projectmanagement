﻿** Met en surbrillance
     Tables à reconstruire
       Aucun
     Index ordonnés en clusters à supprimer
       Aucun
     Index ordonnés en clusters à créer.
       Aucun
     Problèmes de données possibles
       Aucun

** Actions de l'utilisateur
     Créer
       [dbo].[Project] (Table)
       [dbo].[Status] (Table)
       [dbo].[Task] (Table)
       Contrainte par défaut: contrainte sans nom sur [dbo].[Task] (Contrainte par défaut)
       Clé étrangère: contrainte sans nom sur [dbo].[Status] (Clé étrangère)
       Clé étrangère: contrainte sans nom sur [dbo].[Task] (Clé étrangère)
       [dbo].[TR_Tag] (Déclencheur)

** Actions de support

Les bases de données du projet et cible ont des paramètres de classement différents. Des erreurs de déploiement peuvent se produire.

