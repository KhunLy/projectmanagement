﻿/*
Script de déploiement pour ProjectManagement

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "ProjectManagement"
:setvar DefaultFilePrefix "ProjectManagement"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ANSI_NULLS ON,
                ANSI_PADDING ON,
                ANSI_WARNINGS ON,
                ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                QUOTED_IDENTIFIER ON,
                ANSI_NULL_DEFAULT ON,
                CURSOR_DEFAULT LOCAL 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET PAGE_VERIFY NONE 
            WITH ROLLBACK IMMEDIATE;
    END


GO
ALTER DATABASE [$(DatabaseName)]
    SET TARGET_RECOVERY_TIME = 0 SECONDS 
    WITH ROLLBACK IMMEDIATE;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE (CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 367)) 
            WITH ROLLBACK IMMEDIATE;
    END


GO
/*
 Modèle de script de pré-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront exécutées avant le script de compilation.	
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de pré-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de pré-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
GO

GO
PRINT N'Création de [dbo].[Project]...';


GO
CREATE TABLE [dbo].[Project] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [Title]        VARCHAR (255) NOT NULL,
    [CreationDate] DATETIME2 (7) NOT NULL,
    [Deleted]      BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
PRINT N'Création de [dbo].[Project_User]...';


GO
CREATE TABLE [dbo].[Project_User] (
    [Id]        INT IDENTITY (1, 1) NOT NULL,
    [ProjectID] INT NOT NULL,
    [RoleID]    INT NULL,
    [UserID]    INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Création de [dbo].[Role]...';


GO
CREATE TABLE [dbo].[Role] (
    [ID]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
PRINT N'Création de [dbo].[Status]...';


GO
CREATE TABLE [dbo].[Status] (
    [ID]        INT           IDENTITY (1, 1) NOT NULL,
    [Title]     VARCHAR (255) NOT NULL,
    [Weight]    INT           NOT NULL,
    [Color]     VARCHAR (255) NOT NULL,
    [ProjectID] INT           NOT NULL,
    [Deleted]   BIT           NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
PRINT N'Création de [dbo].[Task]...';


GO
CREATE TABLE [dbo].[Task] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [Title]        VARCHAR (255) NOT NULL,
    [Weight]       INT           NOT NULL,
    [Description]  VARCHAR (MAX) NULL,
    [Tag]          INT           NOT NULL,
    [CreationDate] DATETIME2 (7) NOT NULL,
    [Deleted]      BIT           NOT NULL,
    [AssignedToID] INT           NULL,
    [CreatorID]    INT           NULL,
    [StatusID]     INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
PRINT N'Création de [dbo].[User]...';


GO
CREATE TABLE [dbo].[User] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [Lastname]           VARCHAR (255)   NOT NULL,
    [Firstname]          VARCHAR (255)   NOT NULL,
    [Username]           VARCHAR (255)   NOT NULL,
    [Email]              VARCHAR (255)   NOT NULL,
    [Password]           VARBINARY (MAX) NOT NULL,
    [Picture]            VARBINARY (MAX) NULL,
    [InscriptionDate]    DATETIME2 (7)   NOT NULL,
    [LastConnectionDate] DATETIME2 (7)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    UNIQUE NONCLUSTERED ([Email] ASC)
);


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Project]...';


GO
ALTER TABLE [dbo].[Project]
    ADD DEFAULT 0 FOR [Deleted];


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Status]...';


GO
ALTER TABLE [dbo].[Status]
    ADD DEFAULT 0 FOR [Deleted];


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task]
    ADD DEFAULT 0 FOR [Tag];


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task]
    ADD DEFAULT 0 FOR [Deleted];


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Project_User]...';


GO
ALTER TABLE [dbo].[Project_User] WITH NOCHECK
    ADD FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID]);


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Project_User]...';


GO
ALTER TABLE [dbo].[Project_User] WITH NOCHECK
    ADD FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([ID]) ON DELETE SET NULL;


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Project_User]...';


GO
ALTER TABLE [dbo].[Project_User] WITH NOCHECK
    ADD FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID]) ON DELETE CASCADE;


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Status]...';


GO
ALTER TABLE [dbo].[Status] WITH NOCHECK
    ADD FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID]);


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] WITH NOCHECK
    ADD FOREIGN KEY ([AssignedToID]) REFERENCES [dbo].[User] ([ID]) ON DELETE SET NULL;


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] WITH NOCHECK
    ADD FOREIGN KEY ([CreatorID]) REFERENCES [dbo].[User] ([ID]) ON DELETE SET NULL;


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] WITH NOCHECK
    ADD FOREIGN KEY ([StatusID]) REFERENCES [dbo].[Status] ([ID]);


GO
PRINT N'Création de [dbo].[tr_softdelete_project]...';


GO

CREATE TRIGGER [dbo].[tr_softdelete_project]
    ON [dbo].[Project]
    INSTEAD OF DELETE
    AS
    BEGIN
		UPDATE Project SET Deleted = 1
		WHERE ID IN (SELECT ID FROM deleted);
		DELETE FROM [Status] WHERE ProjectID IN (SELECT ID FROM deleted);
	END
GO
PRINT N'Création de [dbo].[tr_softdelete_status]...';


GO

CREATE TRIGGER [dbo].[tr_softdelete_status]
    ON [dbo].[Status]
    INSTEAD OF DELETE
    AS
    BEGIN
		UPDATE [Status] SET Deleted = 1
		WHERE ID IN (SELECT ID FROM deleted);
		DELETE FROM Task WHERE StatusID IN (SELECT ID FROM deleted);
	END
GO
PRINT N'Création de [dbo].[tr_insert_task]...';


GO

CREATE TRIGGER [dbo].[tr_insert_task]
    ON [dbo].[Task]
    AFTER INSERT
    AS
    BEGIN
        SET NoCount ON;
		DECLARE @statusID INT, @id INT;
		DECLARE crs CURSOR FOR SELECT ID, StatusID FROM inserted;
		OPEN crs;
		FETCH crs INTO @id, @statusID;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE Task SET Tag = (
				SELECT MAX(Tag) 
				FROM Task 
				WHERE StatusID in (
					SELECT ID FROM [Status]
					WHERE ProjectID = (
						SELECT ProjectID FROM [Status] 
						WHERE ID = @statusID
					) 
				)
			) + 1
			WHERE ID = @id
			FETCH crs INTO @id, @statusID;
		END
		CLOSE crs;
		DEALLOCATE crs;
    END
GO
PRINT N'Création de [dbo].[tr_softdelete_task]...';


GO

CREATE TRIGGER [dbo].[tr_softdelete_task]
    ON [dbo].[Task]
    INSTEAD OF DELETE
    AS
    BEGIN
		UPDATE Task SET Deleted = 1
		WHERE ID IN (SELECT ID FROM deleted) 
	END
GO
PRINT N'Création de [dbo].[sp_get_tasks_by_project]...';


GO
CREATE PROCEDURE [dbo].[sp_get_tasks_by_project]
	@statusID int
AS
BEGIN
	SELECT * FROM Task t
	WHERE t.StatusID = @statusID;
END
GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
CREATE TABLE [#__checkStatus] (
    id           INT            IDENTITY (1, 1) PRIMARY KEY CLUSTERED,
    [Schema]     NVARCHAR (256),
    [Table]      NVARCHAR (256),
    [Constraint] NVARCHAR (256)
);

SET NOCOUNT ON;

DECLARE tableconstraintnames CURSOR LOCAL FORWARD_ONLY
    FOR SELECT SCHEMA_NAME([schema_id]),
               OBJECT_NAME([parent_object_id]),
               [name],
               0
        FROM   [sys].[objects]
        WHERE  [parent_object_id] IN (OBJECT_ID(N'dbo.Project_User'), OBJECT_ID(N'dbo.Status'), OBJECT_ID(N'dbo.Task'))
               AND [type] IN (N'F', N'C')
                   AND [object_id] IN (SELECT [object_id]
                                       FROM   [sys].[check_constraints]
                                       WHERE  [is_not_trusted] <> 0
                                              AND [is_disabled] = 0
                                       UNION
                                       SELECT [object_id]
                                       FROM   [sys].[foreign_keys]
                                       WHERE  [is_not_trusted] <> 0
                                              AND [is_disabled] = 0);

DECLARE @schemaname AS NVARCHAR (256);

DECLARE @tablename AS NVARCHAR (256);

DECLARE @checkname AS NVARCHAR (256);

DECLARE @is_not_trusted AS INT;

DECLARE @statement AS NVARCHAR (1024);

BEGIN TRY
    OPEN tableconstraintnames;
    FETCH tableconstraintnames INTO @schemaname, @tablename, @checkname, @is_not_trusted;
    WHILE @@fetch_status = 0
        BEGIN
            PRINT N'Vérification de la contrainte : ' + @checkname + N' [' + @schemaname + N'].[' + @tablename + N']';
            SET @statement = N'ALTER TABLE [' + @schemaname + N'].[' + @tablename + N'] WITH ' + CASE @is_not_trusted WHEN 0 THEN N'CHECK' ELSE N'NOCHECK' END + N' CHECK CONSTRAINT [' + @checkname + N']';
            BEGIN TRY
                EXECUTE [sp_executesql] @statement;
            END TRY
            BEGIN CATCH
                INSERT  [#__checkStatus] ([Schema], [Table], [Constraint])
                VALUES                  (@schemaname, @tablename, @checkname);
            END CATCH
            FETCH tableconstraintnames INTO @schemaname, @tablename, @checkname, @is_not_trusted;
        END
END TRY
BEGIN CATCH
    PRINT ERROR_MESSAGE();
END CATCH

IF CURSOR_STATUS(N'LOCAL', N'tableconstraintnames') >= 0
    CLOSE tableconstraintnames;

IF CURSOR_STATUS(N'LOCAL', N'tableconstraintnames') = -1
    DEALLOCATE tableconstraintnames;

SELECT N'Échec de vérification de la contrainte :' + [Schema] + N'.' + [Table] + N',' + [Constraint]
FROM   [#__checkStatus];

IF @@ROWCOUNT > 0
    BEGIN
        DROP TABLE [#__checkStatus];
        RAISERROR (N'Une erreur s''est produite lors de la vérification des contraintes', 16, 127);
    END

SET NOCOUNT OFF;

DROP TABLE [#__checkStatus];


GO
PRINT N'Mise à jour terminée.';


GO
