﻿/*
Script de déploiement pour ProjectManagement

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "ProjectManagement"
:setvar DefaultFilePrefix "ProjectManagement"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
 Modèle de script de pré-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront exécutées avant le script de compilation.	
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de pré-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de pré-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
GO

GO
/*
La colonne [dbo].[Project].[CreationDate] de la table [dbo].[Project] doit être ajoutée mais la colonne ne comporte pas de valeur par défaut et n'autorise pas les valeurs NULL. Si la table contient des données, le script ALTER ne fonctionnera pas. Pour éviter ce problème, vous devez ajouter une valeur par défaut à la colonne, la marquer comme autorisant les valeurs Null ou activer la génération de smart-defaults en tant qu'option de déploiement.
*/

IF EXISTS (select top 1 1 from [dbo].[Project])
    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
/*
La colonne [dbo].[Task].[CreationDate] de la table [dbo].[Task] doit être ajoutée mais la colonne ne comporte pas de valeur par défaut et n'autorise pas les valeurs NULL. Si la table contient des données, le script ALTER ne fonctionnera pas. Pour éviter ce problème, vous devez ajouter une valeur par défaut à la colonne, la marquer comme autorisant les valeurs Null ou activer la génération de smart-defaults en tant qu'option de déploiement.
*/

IF EXISTS (select top 1 1 from [dbo].[Task])
    RAISERROR (N'Lignes détectées. Arrêt de la mise à jour du schéma en raison d''''un risque de perte de données.', 16, 127) WITH NOWAIT

GO
PRINT N'Suppression de [dbo].[tr_softdelete_project]...';


GO
DROP TRIGGER [dbo].[tr_softdelete_project];


GO
PRINT N'Suppression de [dbo].[tr_softdelete_task]...';


GO
DROP TRIGGER [dbo].[tr_softdelete_task];


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[Project]...';


GO
ALTER TABLE [dbo].[Project] DROP CONSTRAINT [DF__Project__Deleted__276EDEB3];


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] DROP CONSTRAINT [DF__Task__Tag__29572725];


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] DROP CONSTRAINT [DF__Task__Deleted__2A4B4B5E];


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[Status]...';


GO
ALTER TABLE [dbo].[Status] DROP CONSTRAINT [FK__Status__ProjectI__2E1BDC42];


GO
PRINT N'Suppression de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] DROP CONSTRAINT [FK__Task__StatusID__2F10007B];


GO
PRINT N'Début de la régénération de la table [dbo].[Project]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Project] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [Title]        VARCHAR (255) NOT NULL,
    [CreationDate] DATETIME2 (7) NOT NULL,
    [Deleted]      BIT           DEFAULT 0 NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Project])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Project] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Project] ([ID], [Title], [Deleted])
        SELECT   [ID],
                 [Title],
                 [Deleted]
        FROM     [dbo].[Project]
        ORDER BY [ID] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Project] OFF;
    END

DROP TABLE [dbo].[Project];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Project]', N'Project';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Début de la régénération de la table [dbo].[Task]...';


GO
BEGIN TRANSACTION;

SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;

SET XACT_ABORT ON;

CREATE TABLE [dbo].[tmp_ms_xx_Task] (
    [ID]           INT           IDENTITY (1, 1) NOT NULL,
    [Title]        VARCHAR (255) NOT NULL,
    [Weight]       INT           NOT NULL,
    [Description]  VARCHAR (MAX) NULL,
    [Tag]          INT           DEFAULT 0 NOT NULL,
    [CreationDate] DATETIME2 (7) NOT NULL,
    [Deleted]      BIT           DEFAULT 0 NOT NULL,
    [AssignedToID] INT           NULL,
    [CreatorID]    INT           NULL,
    [StatusID]     INT           NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Task])
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Task] ON;
        INSERT INTO [dbo].[tmp_ms_xx_Task] ([ID], [Title], [Weight], [Description], [Tag], [Deleted], [StatusID])
        SELECT   [ID],
                 [Title],
                 [Weight],
                 [Description],
                 [Tag],
                 [Deleted],
                 [StatusID]
        FROM     [dbo].[Task]
        ORDER BY [ID] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_Task] OFF;
    END

DROP TABLE [dbo].[Task];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Task]', N'Task';

COMMIT TRANSACTION;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;


GO
PRINT N'Création de [dbo].[Project_User]...';


GO
CREATE TABLE [dbo].[Project_User] (
    [Id]        INT IDENTITY (1, 1) NOT NULL,
    [ProjectID] INT NOT NULL,
    [RoleID]    INT NULL,
    [UserID]    INT NOT NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
PRINT N'Création de [dbo].[Role]...';


GO
CREATE TABLE [dbo].[Role] (
    [ID]   INT           IDENTITY (1, 1) NOT NULL,
    [Name] VARCHAR (255) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    UNIQUE NONCLUSTERED ([Name] ASC)
);


GO
PRINT N'Création de [dbo].[User]...';


GO
CREATE TABLE [dbo].[User] (
    [ID]                 INT             IDENTITY (1, 1) NOT NULL,
    [Lastname]           VARCHAR (255)   NOT NULL,
    [Firstname]          VARCHAR (255)   NOT NULL,
    [Username]           VARCHAR (255)   NOT NULL,
    [Email]              VARCHAR (255)   NOT NULL,
    [Password]           VARBINARY (MAX) NOT NULL,
    [Picture]            VARBINARY (MAX) NULL,
    [InscriptionDate]    DATETIME2 (7)   NOT NULL,
    [LastConnectionDate] DATETIME2 (7)   NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC),
    UNIQUE NONCLUSTERED ([Email] ASC),
    UNIQUE NONCLUSTERED ([Password] ASC)
);


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Status]...';


GO
ALTER TABLE [dbo].[Status] WITH NOCHECK
    ADD FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID]);


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] WITH NOCHECK
    ADD FOREIGN KEY ([StatusID]) REFERENCES [dbo].[Status] ([ID]);


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] WITH NOCHECK
    ADD FOREIGN KEY ([AssignedToID]) REFERENCES [dbo].[User] ([ID]) ON DELETE SET NULL;


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Task]...';


GO
ALTER TABLE [dbo].[Task] WITH NOCHECK
    ADD FOREIGN KEY ([CreatorID]) REFERENCES [dbo].[User] ([ID]) ON DELETE SET NULL;


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Project_User]...';


GO
ALTER TABLE [dbo].[Project_User] WITH NOCHECK
    ADD FOREIGN KEY ([ProjectID]) REFERENCES [dbo].[Project] ([ID]);


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Project_User]...';


GO
ALTER TABLE [dbo].[Project_User] WITH NOCHECK
    ADD FOREIGN KEY ([RoleID]) REFERENCES [dbo].[Role] ([ID]) ON DELETE SET NULL;


GO
PRINT N'Création de contrainte sans nom sur [dbo].[Project_User]...';


GO
ALTER TABLE [dbo].[Project_User] WITH NOCHECK
    ADD FOREIGN KEY ([UserID]) REFERENCES [dbo].[User] ([ID]) ON DELETE CASCADE;


GO
PRINT N'Création de [dbo].[tr_softdelete_project]...';


GO

CREATE TRIGGER [dbo].[tr_softdelete_project]
    ON [dbo].[Project]
    INSTEAD OF DELETE
    AS
    BEGIN
		UPDATE Project SET Deleted = 1
		WHERE ID IN (SELECT ID FROM deleted);
		DELETE FROM [Status] WHERE ProjectID IN (SELECT ID FROM deleted);
	END
GO
PRINT N'Création de [dbo].[tr_insert_task]...';


GO

CREATE TRIGGER [dbo].[tr_insert_task]
    ON [dbo].[Task]
    AFTER INSERT
    AS
    BEGIN
        SET NoCount ON;
		DECLARE @statusID INT, @id INT;
		DECLARE crs CURSOR FOR SELECT ID, StatusID FROM inserted;
		OPEN crs;
		FETCH crs INTO @id, @statusID;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE Task SET Tag = (
				SELECT MAX(Tag) 
				FROM Task 
				WHERE StatusID in (
					SELECT ID FROM [Status]
					WHERE ProjectID = (
						SELECT ProjectID FROM [Status] 
						WHERE ID = @statusID
					) 
				)
			) + 1
			WHERE ID = @id
			FETCH crs INTO @id, @statusID;
		END
		CLOSE crs;
		DEALLOCATE crs;
    END
GO
PRINT N'Création de [dbo].[tr_softdelete_task]...';


GO

CREATE TRIGGER [dbo].[tr_softdelete_task]
    ON [dbo].[Task]
    INSTEAD OF DELETE
    AS
    BEGIN
		UPDATE Task SET Deleted = 1
		WHERE ID IN (SELECT ID FROM deleted) 
	END
GO
PRINT N'Actualisation de [dbo].[sp_get_tasks_by_project]...';


GO
EXECUTE sp_refreshsqlmodule N'[dbo].[sp_get_tasks_by_project]';


GO
PRINT N'Vérification de données existantes par rapport aux nouvelles contraintes';


GO
USE [$(DatabaseName)];


GO
CREATE TABLE [#__checkStatus] (
    id           INT            IDENTITY (1, 1) PRIMARY KEY CLUSTERED,
    [Schema]     NVARCHAR (256),
    [Table]      NVARCHAR (256),
    [Constraint] NVARCHAR (256)
);

SET NOCOUNT ON;

DECLARE tableconstraintnames CURSOR LOCAL FORWARD_ONLY
    FOR SELECT SCHEMA_NAME([schema_id]),
               OBJECT_NAME([parent_object_id]),
               [name],
               0
        FROM   [sys].[objects]
        WHERE  [parent_object_id] IN (OBJECT_ID(N'dbo.Status'), OBJECT_ID(N'dbo.Task'), OBJECT_ID(N'dbo.Project_User'))
               AND [type] IN (N'F', N'C')
                   AND [object_id] IN (SELECT [object_id]
                                       FROM   [sys].[check_constraints]
                                       WHERE  [is_not_trusted] <> 0
                                              AND [is_disabled] = 0
                                       UNION
                                       SELECT [object_id]
                                       FROM   [sys].[foreign_keys]
                                       WHERE  [is_not_trusted] <> 0
                                              AND [is_disabled] = 0);

DECLARE @schemaname AS NVARCHAR (256);

DECLARE @tablename AS NVARCHAR (256);

DECLARE @checkname AS NVARCHAR (256);

DECLARE @is_not_trusted AS INT;

DECLARE @statement AS NVARCHAR (1024);

BEGIN TRY
    OPEN tableconstraintnames;
    FETCH tableconstraintnames INTO @schemaname, @tablename, @checkname, @is_not_trusted;
    WHILE @@fetch_status = 0
        BEGIN
            PRINT N'Vérification de la contrainte : ' + @checkname + N' [' + @schemaname + N'].[' + @tablename + N']';
            SET @statement = N'ALTER TABLE [' + @schemaname + N'].[' + @tablename + N'] WITH ' + CASE @is_not_trusted WHEN 0 THEN N'CHECK' ELSE N'NOCHECK' END + N' CHECK CONSTRAINT [' + @checkname + N']';
            BEGIN TRY
                EXECUTE [sp_executesql] @statement;
            END TRY
            BEGIN CATCH
                INSERT  [#__checkStatus] ([Schema], [Table], [Constraint])
                VALUES                  (@schemaname, @tablename, @checkname);
            END CATCH
            FETCH tableconstraintnames INTO @schemaname, @tablename, @checkname, @is_not_trusted;
        END
END TRY
BEGIN CATCH
    PRINT ERROR_MESSAGE();
END CATCH

IF CURSOR_STATUS(N'LOCAL', N'tableconstraintnames') >= 0
    CLOSE tableconstraintnames;

IF CURSOR_STATUS(N'LOCAL', N'tableconstraintnames') = -1
    DEALLOCATE tableconstraintnames;

SELECT N'Échec de vérification de la contrainte :' + [Schema] + N'.' + [Table] + N',' + [Constraint]
FROM   [#__checkStatus];

IF @@ROWCOUNT > 0
    BEGIN
        DROP TABLE [#__checkStatus];
        RAISERROR (N'Une erreur s''est produite lors de la vérification des contraintes', 16, 127);
    END

SET NOCOUNT OFF;

DROP TABLE [#__checkStatus];


GO
PRINT N'Mise à jour terminée.';


GO
