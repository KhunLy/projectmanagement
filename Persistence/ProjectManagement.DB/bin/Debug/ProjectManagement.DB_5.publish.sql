﻿/*
Script de déploiement pour ProjectManagement

Ce code a été généré par un outil.
La modification de ce fichier peut provoquer un comportement incorrect et sera perdue si
le code est régénéré.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "ProjectManagement"
:setvar DefaultFilePrefix "ProjectManagement"
:setvar DefaultDataPath "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\"
:setvar DefaultLogPath "C:\Program Files\Microsoft SQL Server\MSSQL13.MSSQLSERVER\MSSQL\DATA\"

GO
:on error exit
GO
/*
Détectez le mode SQLCMD et désactivez l'exécution du script si le mode SQLCMD n'est pas pris en charge.
Pour réactiver le script une fois le mode SQLCMD activé, exécutez ce qui suit :
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'Le mode SQLCMD doit être activé de manière à pouvoir exécuter ce script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
/*
 Modèle de script de pré-déploiement							
--------------------------------------------------------------------------------------
 Ce fichier contient des instructions SQL qui seront exécutées avant le script de compilation.	
 Utilisez la syntaxe SQLCMD pour inclure un fichier dans le script de pré-déploiement.			
 Exemple :      :r .\monfichier.sql								
 Utilisez la syntaxe SQLCMD pour référencer une variable dans le script de pré-déploiement.		
 Exemple :      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
DROP TABLE IF EXISTS Task;
DROP TABLE IF EXISTS [Status];
DROP TABLE IF EXISTS Project;
GO

GO
PRINT N'Modification de [dbo].[TR_Tag]...';


GO

ALTER TRIGGER [dbo].[TR_Tag]
    ON [dbo].[Task]
    AFTER INSERT
    AS
    BEGIN
        SET NoCount ON;
		DECLARE @statusID INT, @id INT;
		DECLARE crs CURSOR FOR SELECT ID, StatusID FROM inserted;
		FETCH crs INTO @id, @statusID;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE Task SET Tag = (
				SELECT MAX(Tag) 
				FROM Task 
				WHERE StatusID = @statusID
			) + 1
			WHERE ID = @id
			FETCH crs INTO @id, @statusID;
		END
		CLOSE crs;
		DEALLOCATE crs;
    END
GO
PRINT N'Mise à jour terminée.';


GO
