﻿CREATE TABLE [dbo].[Project]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY,
	[Title] VARCHAR(255) NOT NULL,
	[CreationDate] DATETIME2 NOT NULL,
	[Deleted] BIT NOT NULL DEFAULT 0
)

GO

CREATE TRIGGER [dbo].[tr_softdelete_project]
    ON [dbo].[Project]
    INSTEAD OF DELETE
    AS
    BEGIN
		UPDATE Project SET Deleted = 1
		WHERE ID IN (SELECT ID FROM deleted);
		DELETE FROM [Status] WHERE ProjectID IN (SELECT ID FROM deleted);
	END
