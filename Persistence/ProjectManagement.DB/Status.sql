﻿CREATE TABLE [dbo].[Status]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY,
	[Title] VARCHAR(255) NOT NULL,
	[Weight] INT NOT NULL,
	[Color] VARCHAR(255) NOT NULL,
	[ProjectID] INT NOT NULL
		FOREIGN KEY REFERENCES Project(ID),
	[Deleted] BIT NOT NULL DEFAULT 0
)

GO

CREATE TRIGGER [dbo].[tr_softdelete_status]
    ON [dbo].[Status]
    INSTEAD OF DELETE
    AS
    BEGIN
		UPDATE [Status] SET Deleted = 1
		WHERE ID IN (SELECT ID FROM deleted);
		DELETE FROM Task WHERE StatusID IN (SELECT ID FROM deleted);
	END
