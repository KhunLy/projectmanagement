﻿CREATE TABLE [dbo].[Project_User]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY,
	[ProjectID] INT NOT NULL REFERENCES Project(ID),
	[RoleID] INT NULL REFERENCES [Role](ID) ON DELETE SET NULL,
	[UserID] INT NOT NULL REFERENCES [User](ID) ON DELETE CASCADE
)                                                                                                