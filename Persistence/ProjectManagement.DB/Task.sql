﻿CREATE TABLE [dbo].[Task]
(
	[ID] INT NOT NULL PRIMARY KEY IDENTITY,
	[Title] VARCHAR(255) NOT NULL,
	[Weight] INT NOT NULL,
	[Description] VARCHAR(MAX),
	[Tag] INT NOT NULL DEFAULT 0,
	[CreationDate] DATETIME2 NOT NULL,
	[Deleted] BIT NOT NULL DEFAULT 0,
	[AssignedToID] INT NULL REFERENCES [User](ID),
	[CreatorID] INT NULL REFERENCES [User](ID),
	[StatusID] INT NOT NULL
		FOREIGN KEY REFERENCES [Status](ID)
)

GO

CREATE TRIGGER [dbo].[tr_insert_task]
    ON [dbo].[Task]
    AFTER INSERT
    AS
    BEGIN
        SET NoCount ON;
		DECLARE @statusID INT, @id INT;
		DECLARE crs CURSOR FOR SELECT ID, StatusID FROM inserted;
		OPEN crs;
		FETCH crs INTO @id, @statusID;
		WHILE @@FETCH_STATUS = 0
		BEGIN
			UPDATE Task SET Tag = (
				SELECT MAX(Tag) 
				FROM Task 
				WHERE StatusID in (
					SELECT ID FROM [Status]
					WHERE ProjectID = (
						SELECT ProjectID FROM [Status] 
						WHERE ID = @statusID
					) 
				)
			) + 1
			WHERE ID = @id
			FETCH crs INTO @id, @statusID;
		END
		CLOSE crs;
		DEALLOCATE crs;
    END

GO

CREATE TRIGGER [dbo].[tr_softdelete_task]
    ON [dbo].[Task]
    INSTEAD OF DELETE
    AS
    BEGIN
		UPDATE Task SET Deleted = 1
		WHERE ID IN (SELECT ID FROM deleted) 
	END