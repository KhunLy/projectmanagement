﻿CREATE PROCEDURE [dbo].[sp_get_tasks_by_project]
	@statusID int
AS
BEGIN
	SELECT * FROM Task t
	WHERE t.StatusID = @statusID;
END
