﻿using Autofac;
using ProjectManagement.API.DependencyInjection;
using ProjectManagement.API.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using ToolBox.Token;

namespace ProjectManagement.API.Filters
{
    public class ApiAuthorizeAttribute: AuthorizeAttribute
    {
        public override void OnAuthorization(HttpActionContext actionContext)
        {
            actionContext.Request.Headers.TryGetValues("Authorization", out IEnumerable<string> values);
            string token = values?.FirstOrDefault(v => v.StartsWith("Bearer "))
                .Replace("Bearer ", string.Empty);
            if (token == null)
                throw new UnauthorizedException("No Authorization Bearer Found");
            try
            {
                ClaimsPrincipal principal = Resources.Container.Resolve<ITokenService>().ValidateToken(token);
                int id = int.Parse(principal.Claims.FirstOrDefault(c => c.Type == "ID").Value);
                actionContext.ControllerContext.RouteData.Values.Add("connectedUserId", id);
            }
            catch (Exception)
            {
                throw new UnauthorizedException("Invalid Authorization Bearer");
            }
        }
    }
}