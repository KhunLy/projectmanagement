﻿using Autofac;
using ProjectManagement.API.DependencyInjection;
using ProjectManagement.API.Exceptions;
using ProjectManagement.API.Models;
using ProjectManagement.BLL.Google;
using ProjectManagement.BLL.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ToolBox.Token;
using ToolBox.Mapper;
using ProjectManagement.BLL.Models;
using ProjectManagement.API.Filters;

namespace ProjectManagement.API.Controllers.API
{
    public class SecurityController : BaseApiController
    {
        [HttpPost]
        [Route("api/GoogleLogin")]
        public async Task<string> GoogleLogin([FromBody]string token)
        {
            GooglePayload payload = await Resources.Container.Resolve<IGoogleValidator>().ValidateToken(token);
            UserDTO user = UOW.GetRepository<IUserRepository>().GetByEmail(payload.Email).MapTo<UserDTO>();
            if (user == null)
                UOW.GetRepository<IUserRepository>().Insert(payload.MapTo<User>());
            return Resources.Container.Resolve<ITokenService>().GenerateToken(user);
        }

        [HttpPost]
        [Route("api/Login")]
        public string Login([FromBody]LoginDTO login)
        {
            UserDTO user = UOW.GetRepository<IUserRepository>().GetByEmailAndPassword(login.Email, login.Password).MapTo<UserDTO>();
            if (user == null)
                throw new UnauthorizedException("Bad Credentials");
            return Resources.Container.Resolve<ITokenService>().GenerateToken(user);
        }
    }
}
