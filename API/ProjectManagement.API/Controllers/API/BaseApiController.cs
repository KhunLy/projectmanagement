﻿using ProjectManagement.BLL.UOW;
using System.Web.Http;

namespace ProjectManagement.API.Controllers.API
{
    public abstract class BaseApiController : ApiController
    {
        protected UnitOfWork UOW { get; }
        public BaseApiController()
        {
            UOW = new UnitOfWork();
        }
        protected override void Dispose(bool disposing)
        {
            UOW.Dispose();
            base.Dispose(disposing);
        }
    }
}