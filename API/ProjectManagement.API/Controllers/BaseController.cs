﻿using ProjectManagement.BLL.UOW;
using System.Web.Mvc;

namespace ProjectManagement.API.Controllers
{
    public abstract class BaseController : Controller
    {
        protected UnitOfWork UOW { get; }
        public BaseController()
        {
            UOW = new UnitOfWork();
        }
        protected override void Dispose(bool disposing)
        {
            UOW.Dispose();
            base.Dispose(disposing);
        }
    }
}
