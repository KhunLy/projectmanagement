﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using ProjectManagement.BLL.Google;
using ToolBox.Token;

namespace ProjectManagement.API.DependencyInjection
{
    public class Resources
    {
        private static readonly Lazy<IContainer> _Container = 
            new Lazy<IContainer>(() => _Builder.Build());
        public static IContainer Container
        {
            get { return _Container.Value; }
        }

        private static ContainerBuilder _Builder;
        static Resources()
        {
            RegisterServices();
        }

        private static void RegisterServices()
        {
            _Builder = new ContainerBuilder();
            _Builder.RegisterType<GoogleValidator>().As<IGoogleValidator>().SingleInstance();
            _Builder.RegisterType<TokenService>().As<ITokenService>().SingleInstance();
        }
    }
}