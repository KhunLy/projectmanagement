﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace ProjectManagement.API.Exceptions
{
    public class BadRequestException : HttpResponseException
    {
        public BadRequestException(object obj = null) : base(HttpStatusCode.BadRequest)
        {
            string content = Newtonsoft.Json.JsonConvert.SerializeObject(obj);
            Response.Content = new StringContent(content, Encoding.UTF8, "application/json");
        }
    }
}