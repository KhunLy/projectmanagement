﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http;

namespace ProjectManagement.API.Exceptions
{
    public class UnauthorizedException : HttpResponseException
    {
        public UnauthorizedException(string message = null) : base(HttpStatusCode.Unauthorized)
        {
            string content = Newtonsoft.Json.JsonConvert.SerializeObject(new { ErrorMessage = message });
            Response.Content = new StringContent(content, Encoding.UTF8, "application/json");
        }
    }
}