﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.API.Models
{
    public class TaskDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public int Weight { get; set; }
        public string Description { get; set; }
        public int Tag { get; set; }
        public int StatusID { get; set; }
        public bool Deleted { get; set; }
    }
}