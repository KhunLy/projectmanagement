﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProjectManagement.API.Models
{
    public class UserDTO
    {
        public int ID { get; set; }
        public string Lastname { get; set; }
        public string Firstname { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public byte[] Picture { get; set; }
        public DateTime InscriptionDate { get; set; }
        public DateTime? LastConnectionDate { get; set; }
    }
}