import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SecurityModule} from './security/security.module';
import {MainModule} from './main/main.module';

const routes: Routes = [
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
