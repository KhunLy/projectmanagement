import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SecurityRoutingModule } from './security-routing.module';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { SecurityComponent } from './security.component';
import { MaterialModule } from '../material/material.module';
import { FormsModule } from '@angular/forms';
import { AuthServiceConfig, GoogleLoginProvider, SocialLoginModule } from 'angular-6-social-login'

export function getAuthServiceConfigs() {
  let config = new AuthServiceConfig(
    [
      {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider("509648228972-fku01v56l859cqhn2g32oqbrnq1jtjji.apps.googleusercontent.com")
      }
    ]
  );
  return config;
}

@NgModule({
  declarations: [LoginComponent, RegisterComponent, SecurityComponent],
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    SecurityRoutingModule,
    SocialLoginModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: getAuthServiceConfigs
    }
  ],
})
export class SecurityModule { }
