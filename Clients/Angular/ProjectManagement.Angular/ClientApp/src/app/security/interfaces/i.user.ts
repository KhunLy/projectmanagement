export interface IUser {
  ID: number;
  Username: string;
  Email: string;
  Password: string;
}
