import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { MainComponent } from './main.component';
import {MaterialModule} from '../material/material.module';
import { ProjectComponent } from './project/project.component';
import { StatusDialogComponent } from './project/status-dialog/status-dialog.component';
import { TaskDialogComponent } from './project/task-dialog/task-dialog.component';
import {MAT_DIALOG_DEFAULT_OPTIONS} from '@angular/material';
import {FormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    MainComponent,
    ProjectComponent,
    StatusDialogComponent,
    TaskDialogComponent
  ],
  entryComponents: [
    StatusDialogComponent,
    TaskDialogComponent
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    MaterialModule,
    FormsModule
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true, width: '700px', margin: '0 auto'}}
  ]
})
export class MainModule { }
