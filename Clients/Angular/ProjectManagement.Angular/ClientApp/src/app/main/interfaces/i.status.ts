import {ITask} from './i.task';

export interface IStatus {
  ID: number;
  Title: string;
  Color: string;
  Weight: number;
  Tasks: Array<ITask>;
  ProjectID: number;
}
