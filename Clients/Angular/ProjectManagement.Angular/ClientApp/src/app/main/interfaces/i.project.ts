import {IStatus} from './i.status';

export interface IProject {
  ID: number;
  Title: string;
  StatusList: Array<IStatus>;
}
