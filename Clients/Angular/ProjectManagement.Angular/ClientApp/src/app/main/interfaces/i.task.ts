import {IComments} from './i.comments';
import {IUser} from '../../security/interfaces/i.user';

export interface ITask {
  ID: number;
  Title: string;
  Weight: number;
  StatusID: number;
  Description: string;
  Tag: number;
  AssignToUserID: number;
  AssignToUser: IUser;
  AuthorID: number;
  Author: IUser;
  Comments: Array<IComments>;
}
