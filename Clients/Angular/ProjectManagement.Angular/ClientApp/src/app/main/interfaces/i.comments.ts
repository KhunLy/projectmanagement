import {IUser} from '../../security/interfaces/i.user';

export interface IComments {
  ID: number;
  Content: string;
  AuthorID: number;
  Author: IUser;
  TaskID: number;
}
