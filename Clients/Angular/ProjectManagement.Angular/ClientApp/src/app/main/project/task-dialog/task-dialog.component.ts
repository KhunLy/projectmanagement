import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ITask} from '../../interfaces/i.task';

@Component({
  selector: 'app-task-dialog',
  templateUrl: './task-dialog.component.html',
  styleUrls: ['./task-dialog.component.sass']
})
export class TaskDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<TaskDialogComponent>, @Inject(MAT_DIALOG_DATA) public Task: ITask = null) {}

  ngOnInit() {
    if (this.Task == null) {
      this.Task = <ITask>{};
    }
  }

  onCancel() {
    this.dialogRef.close();
  }

  deleteTask(task: ITask) {
    this.dialogRef.close({Result: 'delete', Object: task});
  }
}
