import { Component, OnInit } from '@angular/core';
import {IProject} from '../interfaces/i.project';
import {CdkDragDrop, moveItemInArray, transferArrayItem} from '@angular/cdk/drag-drop';
import {MatDialog, MatDialogConfig} from '@angular/material';
import {StatusDialogComponent} from './status-dialog/status-dialog.component';
import {TaskDialogComponent} from './task-dialog/task-dialog.component';
import {IStatus} from '../interfaces/i.status';
import {ITask} from '../interfaces/i.task';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.sass'],
})
export class ProjectComponent implements OnInit {

  constructor(public dialog: MatDialog) { }
  private Project: IProject;

  private Compteur: number;

  private TaskCompteur: number;

  ngOnInit() {
    this.Project = <IProject>{Title: 'My Super Project', StatusList: []};
    this.Compteur = 0;
    this.TaskCompteur = 0;
  }

  dropTask(event: CdkDragDrop<ITask[]>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      transferArrayItem(event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex);
        event.container.data[event.currentIndex].StatusID
          = parseInt(event.container.id.replace('task-container-', ''), null);
        console.log(event.container.data[event.currentIndex]);
    }
  }

  openAddStatusDialog() {
    this.dialog.open(StatusDialogComponent)
      .afterClosed().subscribe((status) => {
        if (status != null) {
          status.ID = ++this.Compteur;
          this.Project.StatusList.push(status);
        }
      }
    );
  }

  openTaskDialog(status: IStatus, task: ITask = null) {
    this.dialog.open(TaskDialogComponent, <MatDialogConfig>{data: task})
        .afterClosed().subscribe((data) => {
          if (data != null) {
            if (data.Result === 'delete') {
              status.Tasks
                = status.Tasks.filter(x => x !== data.Object);
            } else {
              data.StatusID = status.ID;
              if (data.ID == null) {
                data.ID = ++this.TaskCompteur;
                status.Tasks.push(data);
              }
            }
          }
      }
    );
  }

  statusIdsMap(status: IStatus) {
    return 'task-container-' + status.ID;
  }
}
