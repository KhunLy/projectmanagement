import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {IStatus} from '../../interfaces/i.status';

@Component({
  selector: 'app-status-dialog',
  templateUrl: './status-dialog.component.html',
  styleUrls: ['./status-dialog.component.sass']
})
export class StatusDialogComponent implements OnInit {

  private Colors: Array<string>;

  private Status: IStatus;

  constructor(public dialogRef: MatDialogRef<StatusDialogComponent>) { }


  ngOnInit() {
    this.Colors = [
      'red',
      'pink',
      'purple',
      'deep-purple',
      'indigo',
      'blue',
      'light-blue',
      'cyan',
      'teal',
      'green',
      'light-green',
      'lime',
      'yellow',
      'amber',
      'orange',
      'deep-orange',
      'brown',
      'grey',
      'blue-grey'
    ];
    this.Status = <IStatus>{Tasks: []};
  }

  onCancel() {
    this.dialogRef.close();
  }

}
