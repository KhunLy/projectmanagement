import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {MainComponent} from './main.component';
import {ProjectComponent} from './project/project.component';

const routes: Routes = [
  { path: '', component: MainComponent, children: [
    { path: 'project', component: ProjectComponent }
  ]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule { }
